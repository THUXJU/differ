package com.chackman.extractor;

import com.chackman.utils.Watchman;
import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: yusup
 * Date: 19/05/14
 * Time: 23:35
 */
public class FileExtractor extends SimpleFileVisitor<Path> {


    public static List<File> symbolicLinks = new ArrayList<File>();
    public static List<File> compressedFiles = new ArrayList<File>();
    public static List<File> singleFiles = new ArrayList<File>();
    public static List<File> directories = new ArrayList<File>();

    private File rootDir;
    private File workingDir;
    private File targetDir;
    private File compressedDir;
    private File malcompressedDir;

    public FileExtractor(File rootDir, File targetDir, File workingDir, File compressedDir, File malCompressedDir) {
        this.rootDir = rootDir;
        this.targetDir = targetDir;
        this.workingDir = workingDir;
        this.compressedDir = compressedDir;
        this.malcompressedDir = malCompressedDir;
    }


    public File getCompressedDir() {
        return compressedDir;
    }

    public File getWorkingDir() {
        return workingDir;
    }

    public void setWorkingDir(File workingDir) {
        this.workingDir = workingDir;
    }

    public void clearDirectory(File folder) {
        File[] files = folder.listFiles();
        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            if (file.exists()) {
                if (file.isDirectory()) {
                    if (file.listFiles() != null && file.listFiles().length == 0) {
                        System.out.println("deleting direcotry :" + file.getAbsolutePath());
                        file.delete();
                    } else if (file.listFiles() != null) {
                        clearDirectory(file);
                        System.out.println("deleting directory:" + file.getAbsolutePath());
                        file.delete();
                    }
                } else {
                    System.out.println("deleting file:" + file.getAbsolutePath());
                    file.delete();
                }
            } else {
                System.out.println("file already hsa been deleted mate!");
            }
        }
    }

    public void flushSingleFiles() {
        while (singleFiles.size() > 0) {
            File readyFile = singleFiles.remove(0);
            try {
                FileUtils.copyFile(readyFile, new File(targetDir.getAbsolutePath() + File.separator +
                        generateRandomString() + "_" + readyFile
                        .getName()));
                readyFile.delete();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if (attrs.isSymbolicLink()) {
//            System.out.format("Symbolic link: %s", file);
            symbolicLinks.add(file.toFile());
        } else if (attrs.isRegularFile()) {
//            System.out.format("Regular File: %s", file);
            if (CompressionType.isCompressedFile(file.toFile().getName())) {
                FileUtils.copyFile(file.toFile(), new File(compressedDir.getAbsolutePath() + File.separator +
                        generateRandomString() + "." +
                        CompressionType.getType(file.toFile().getName()).getSuffix()));
                file.toFile().delete();
            } else {
                singleFiles.add(file.toFile());
            }
        } else {
            System.out.format("Other: %s", file);
        }
//        System.out.println("(" + attrs.size() + "bytes)");
        return FileVisitResult.CONTINUE;
    }

    public boolean isMetaDirectory(File dir) {
        return isEqual(dir, workingDir) || isEqual(dir, targetDir)
                || isEqual(dir, compressedDir) || isEqual(dir, malcompressedDir);
    }


    public boolean isEqual(File a, File b) {
        if (a == null || b == null) {
            return false;
        }
        return a.getAbsolutePath().equals(b.getAbsolutePath());
    }

    public void clearFileList(List<File> directories) {
        for (int i = 0; i < directories.size(); i++) {
            File file = directories.get(i);
            if (isRootFolder(file)) {
                // as list returns an abstract list not a workable list by all means, so you need to do it by yourself.
                clearFileList(new ArrayList<File>(Arrays.asList(file.listFiles())));
            }
            if (!isMetaDirectory(file) && !isRootFolder(file))
                clearDirectory(file);
        }
        directories.clear();
    }

    private boolean isRootFolder(File file) {
        if (file == null)
            return false;

        if (file.getAbsolutePath().equals(rootDir.getAbsolutePath()))
            return true;
        return false;
    }


    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        if (isMetaDirectory(dir.toFile())) {
//            System.out.println(" we should be looking into this sorry ! " + dir.toFile().getAbsolutePath());
        } else {
            directories.add(dir.toFile());
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
        System.out.println(exc);
        return FileVisitResult.CONTINUE;
    }


    public void printStastics() {
        System.out.println("single files : " + singleFiles.size());
        System.out.println("directory files : " + directories.size());
        System.out.println("symbolic links :  " + symbolicLinks.size());
        System.out.println("compressed files : " + compressedFiles.size());
    }


    public void printCompressedFiles() {
        Iterator<File> iterator = compressedFiles.iterator();
        while (iterator.hasNext()) {
            File file = iterator.next();
            System.out.println(file.getName());
        }
    }

    public void printDirectories() {
        Iterator<File> iterator = directories.iterator();
        while (iterator.hasNext()) {
            File next = iterator.next();
            System.out.println(next.getName());
        }
    }


    public static String generateRandomString() {
        return Long.toHexString(Double.doubleToLongBits(Math.random()));
    }


    static class CompressedFileConsumer extends Thread {
        private FileExtractor fileProcessor;


        public CompressedFileConsumer(FileExtractor fileProcessor) {
            this.fileProcessor = fileProcessor;

        }

        public void decompression(File file) {
            if (file == null || !file.exists()) {
                return;
            }
            try {
                StringBuilder command = new StringBuilder().append
                        (" WinRAR x -inul ")
                        .append(fileProcessor.getCompressedDir().getAbsolutePath()
                        ).append(File.separator).append(file.getName()).append
                                (" *.* ")
                        .append
                                (fileProcessor.getWorkingDir().getAbsolutePath()).append(File.separator);
                System.out.println(command.toString());
                Process exec = Runtime.getRuntime().exec(command.toString(), null, fileProcessor.getWorkingDir());
                BufferedReader reader = new BufferedReader(new InputStreamReader(exec.getInputStream()));
                String line;
                StringBuilder outputBuffer = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    outputBuffer.append(line);
                }
                System.out.println(outputBuffer.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            compressedFiles.clear();
            if (fileProcessor.compressedDir.listFiles() != null)
                Collections.addAll(compressedFiles, fileProcessor.compressedDir.listFiles());
            while (compressedFiles.size() > 0) {
                File file = compressedFiles.remove(0);

                if (file != null && file.exists()) {
                    // do the decompress process now.
                    CompressionType compressionType = CompressionType.getType(file.getName());
                    Watchman watchman = new Watchman(fileProcessor.getWorkingDir().getAbsolutePath().toString());
                    watchman.start();
                    switch (compressionType) {
                        case ZIP:
                            System.out.println("decompressing zip file now:");
                            decompression(file);
                            break;
                        case RAR:
                            System.out.println("decompressing rar file now:");
                            decompression(file);
                            break;
                        default:
                            System.out.println("unsupported file type :" + compressionType.getSuffix());
                            break;
                    }
                    watchman.kill();
                    watchman.interrupt();
                    if (watchman.isFolderChanged()) {  // there is something wrong with the compressed files.
                        try {
                            Files.walkFileTree(Paths.get(fileProcessor.getWorkingDir().getAbsolutePath()), fileProcessor);
                            fileProcessor.flushSingleFiles();
                            fileProcessor.clearDirectory(fileProcessor.getWorkingDir());
                            file.delete();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            FileUtils.copyFile(file, new File(fileProcessor.malcompressedDir.getAbsolutePath() + File.separator
                                    + file.getName()));
                            file.delete();
                            System.out.println("nothing has been happend , so you decompression process is problematic.");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                }
                compressedFiles.clear();
                if (fileProcessor.compressedDir.listFiles() != null)
                    Collections.addAll(compressedFiles, fileProcessor.compressedDir.listFiles());
            }
            fileProcessor.getWorkingDir().delete();
            fileProcessor.getCompressedDir().delete();
            System.out.println("job is done exiting...");
        }
    }

    private boolean isWorkingDirectoryEmpty() {
        File[] files = workingDir.listFiles();
        if (files == null || files.length == 0) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {


        if (args == null || args.length == 0) {
            System.out.println("Please input correct directory for extracting !");
            return;
        }


        String root = args[0];


        String prefix = FileExtractor.generateRandomString();


        String targetDirectory = root + File.separator + prefix + "_target";
        String workingDirectory = root + File.separator + prefix + "_working";
        String compressedFilesDir = root + File.separator + prefix + "_compressed";
        String malCompressedFilesDir = root + File.separator + prefix + "_malcompressed";

        File targetDir = new File(targetDirectory);
        File workingDir = new File(workingDirectory);
        File compressedDir = new File(compressedFilesDir);
        File malCompressedDir = new File(malCompressedFilesDir);
        File rootDir = new File(root);

        if (!targetDir.exists())
            targetDir.mkdir();
        if (!workingDir.exists())
            workingDir.mkdir();
        if (!compressedDir.exists())
            compressedDir.mkdir();
        if (!malCompressedDir.exists())
            malCompressedDir.mkdirs();

        FileExtractor fileProcessor = new FileExtractor(rootDir, targetDir,
                workingDir, compressedDir, malCompressedDir);

        try {
            Files.walkFileTree(Paths.get(root), fileProcessor);
            fileProcessor.flushSingleFiles();
            fileProcessor.clearFileList(directories);
            CompressedFileConsumer compressedFileConsumer = new CompressedFileConsumer(fileProcessor);
            compressedFileConsumer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


