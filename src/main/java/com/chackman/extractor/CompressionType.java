package com.chackman.extractor;

import org.apache.commons.lang.StringUtils;

/**
 * Created with IntelliJ IDEA.
 * User: yusup
 * Date: 20/05/14
 * Time: 18:24
 */
public enum CompressionType {
    ZIP("zip"), TAR("tar"), TARGZ("tar.gz"), TARGZIP("tar.gzip"), RAR("rar"), GZ("gz"), BZ2("bz2"), TARBZ2("tar.bz2");



    public static CompressionType getType(String file) {
        if (StringUtils.isBlank(file)) {
            return null;
        }
        CompressionType[] values = CompressionType.values();
        for (int i = 0; i < values.length; i++) {
            CompressionType value = values[i];
            if (file.toLowerCase().endsWith(value.getSuffix().toLowerCase())) {
                return value;
            }
        }
        return null;
    }



    public static boolean isCompressedFile(String fileName) {
        if (StringUtils.isBlank(fileName)) {
            return false;
        }

        CompressionType[] values = CompressionType.values();
        for (int i = 0; i < values.length; i++) {
            CompressionType value = values[i];
            if (fileName.toLowerCase().endsWith(value.getSuffix().toLowerCase())) {
                return true;
            }
        }
        return false;

    }

    private String suffix;


    public String getSuffix() {
        return suffix;
    }

    private CompressionType(String suffix) {
        this.suffix = suffix;
    }
}
