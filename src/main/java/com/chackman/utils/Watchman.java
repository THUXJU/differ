package com.chackman.utils;

import com.sun.nio.file.ExtendedWatchEventModifier;

import java.io.IOException;
import java.nio.file.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: yusup
 * Date: 26/05/14
 * Time: 15:55
 * This is for watching directories for various purposes.
 */
public class Watchman extends Thread {

    WatchService watchService;
    private Path path;
    private boolean folderChanged = false;
    private boolean kill = false;

    public boolean isFolderChanged() {
        return folderChanged;
    }

    public Watchman(String folder) {
        try {
            System.out.println("watching for folder :" + folder);
            path = Paths.get(folder);
            watchService = path.getFileSystem().newWatchService();

            WatchEvent.Kind[] events = new WatchEvent.Kind[]{
                    StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_DELETE,
                    StandardWatchEventKinds.ENTRY_MODIFY
            };
            path.register(watchService, events
                    );

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void kill() {
        kill = true;
    }

    @Override
    public void run() {
        while (!kill) {
            try {
                WatchEvent.Kind kind = null;
                final WatchKey key = watchService.take();
                List<WatchEvent<?>> events = key.pollEvents();
                for (WatchEvent event : events) {
                    kind = event.kind();
                    if (StandardWatchEventKinds.OVERFLOW == kind) {
                        System.out.println("overflow happening.");
                        continue;
                    } else if (kind == StandardWatchEventKinds.ENTRY_CREATE) {
                        folderChanged = true;
                        Path newPath = (Path) event.context();
                        System.out.println("new path created:" + newPath.toFile().getAbsolutePath());
                    } else if (kind == StandardWatchEventKinds.ENTRY_DELETE) {
                        folderChanged = true;
                        Path newPath = (Path) event.context();
                        System.out.println("file has been deleted:" + newPath.toFile().getAbsolutePath());
                        if (path.toFile().getAbsolutePath().equals(newPath.toFile().getAbsolutePath())) {// this
                            // means we deleted working directory job is done.
                            kill = true;
                            return;
                        }
                    } else if (kind == StandardWatchEventKinds.ENTRY_MODIFY) {
                        folderChanged = true;
                        Path newPath = (Path) event.context();
                        System.out.println("File has been modified:" + newPath.toFile().getAbsolutePath());
                    }
                }
                key.reset();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
        }
    }

    //start watching folder
    public void init() {
    }

    public static void main(String[] args) {

        Watchman watchman = new Watchman("/Users/yusup");
        watchman.start();
    }

}
