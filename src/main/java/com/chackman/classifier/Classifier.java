package com.chackman.classifier;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: yusup
 * Date: 22/05/14
 * Time: 18:38
 */
public class Classifier {

    private File source;
    private File destination;

    public Classifier(File src, File dst) {
        this.source = src;
        this.destination = dst;
    }

    public File getSource() {
        return source;
    }

    public void setSource(File source) {
        this.source = source;
    }

    public File getDestination() {
        return destination;
    }

    public void setDestination(File destination) {
        this.destination = destination;
    }

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            System.out.println(" please input source and target directory");
            return;
        }
        String source = args[0];
        String target = args[1];

        System.out.println("source is :" + source);
        System.out.println("target is :" + target);


        File src = new File(source);
        File dst = new File(target);

        if (!(src.exists() && dst.exists())) {
            System.out.println(" please input effective directories!");
            return;
        }

        Classifier classifier = new Classifier(src, dst);
        Set<String> extentions = new HashSet<String>();
        File[] files = src.listFiles();

        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            extentions.add(getFileExtention(file.getName()).toLowerCase());
        }

        Iterator<String> iterator = extentions.iterator();
        while (iterator.hasNext()) {
            File tempDir = new File(classifier.getDestination().getAbsolutePath() + File.separator + iterator.next());
            if (tempDir.exists()) {

            } else {
                tempDir.mkdirs();
            }
        }

        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            File targetFile = new File(classifier.getDestination().getAbsolutePath() + File.separator +
                    getFileExtention(file.getName()) + File.separator + file.getName());
            try {
                FileUtils.copyFile(file, targetFile);
                file.delete();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private static String getFileExtention(String name) {
        String s = "default";
        if (StringUtils.isBlank(name)) {
            return s;
        }
        if (name.lastIndexOf(".")  == -1 ) {
            return s;
        }
        return name.substring(name.lastIndexOf(".") + 1).toLowerCase()  ;
    }
}
