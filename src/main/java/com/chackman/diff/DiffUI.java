package com.chackman.diff;

import org.apache.commons.io.FileUtils;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: yusup
 * Date: 4/21/14
 * Time: 16:45
 */
public class DiffUI extends JFrame {
    private JPanel mainPanel;
    private JButton searchBtn;
    private JTextField directoryPath;
    private JTextPane resultsPane;
    private JScrollPane jscrollPane;
    private JButton extractButton;
    private StyledDocument document;


    private final JFileChooser fileChooser = new JFileChooser();
    private File chosenfile;


    public static final String defaultDirectory = System.getProperty("user.home");


    private void initColumnSizes(JTable table) {
        FileTableModel model = (FileTableModel) table.getModel();
        TableColumn column = null;
        Component comp = null;
        int headerWidth = 0;
        int cellWidth = 0;
        Object[] longValues = model.longValues;
        TableCellRenderer headerRenderer =
                table.getTableHeader().getDefaultRenderer();

        for (int i = 0; i < 4; i++) {
            column = table.getColumnModel().getColumn(i);

            comp = headerRenderer.getTableCellRendererComponent(
                    null, column.getHeaderValue(),
                    false, false, 0, 0);
            headerWidth = comp.getPreferredSize().width;

            comp = table.getDefaultRenderer(model.getColumnClass(i)).
                    getTableCellRendererComponent(
                            table, longValues[i],
                            false, false, 0, i);
            cellWidth = comp.getPreferredSize().width;


            column.setPreferredWidth(Math.max(headerWidth, cellWidth));
        }
    }


    public void alert(String message) {
        JOptionPane.showMessageDialog(this, message);
    }

    public void setUpSportColumn(JTable table,
                                 TableColumn sportColumn) {
        //Set up the editor for the sport cells.
        JComboBox comboBox = new JComboBox();
        comboBox.addItem("Snowboarding");
        comboBox.addItem("Rowing");
        comboBox.addItem("Knitting");
        comboBox.addItem("Speed reading");
        comboBox.addItem("Pool");
        comboBox.addItem("None of the above");
        sportColumn.setCellEditor(new DefaultCellEditor(comboBox));

        //Set up tool tips for the sport cells.
        DefaultTableCellRenderer renderer =
                new DefaultTableCellRenderer();
        renderer.setToolTipText("Click for combo box");
        sportColumn.setCellRenderer(renderer);
    }


    /**
     * we are gonna do some basic shitty initialization for now...
     */
    public void init() {
        final DiffUI ui = this;

        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        directoryPath.setText(defaultDirectory);


        document = resultsPane.getStyledDocument();
        /**
         *  ActionPotential 需要增加一个目录展示的表格界面
         */


        ui.extractButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Map<Long, List<File>> uniqueFilesMap = DirectoryMacho.getUniqueFilesMap();
                if (uniqueFilesMap.isEmpty()) {
                    alert("请选择文件目录后再提取质文件!");
                } else {
                    File output = new File(ui.chosenfile.getAbsolutePath() + "-unique");
                    if (!output.exists()) {
                        output.mkdir();
                    }

                    Iterator iterator = uniqueFilesMap.keySet().iterator();
                    while (iterator.hasNext()) {
                        Long fileSize = (Long) iterator.next();
                        List<File> tFileList = uniqueFilesMap.get(fileSize);
                        if (tFileList.size() > 0) {
                            File uniqueFile = tFileList.get(0);
                            File destFile = new File(output.getAbsolutePath() + File.separator + uniqueFile.getName());
                            try {
                                copyFileUsingApacheCommonsIO(uniqueFile, destFile);
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }

                        }

                    }

                    alert("任务完成!");
                    try {
                        Desktop.getDesktop().open(output);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                }
            }
        });


        ui.searchBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int retValue = ui.fileChooser.showOpenDialog(ui);


                if (retValue == JFileChooser.APPROVE_OPTION) {
                    ui.chosenfile = ui.fileChooser.getSelectedFile();
                    if (ui.chosenfile != null) {
                        ui.directoryPath.setText(ui.chosenfile.getAbsolutePath());

                        if (ui.chosenfile.exists()) {
                            if (ui.chosenfile.isDirectory()) {
                                new DirectoryMacho(ui.chosenfile, resultsPane).start();
                            } else {
                                alert("选择的文件不是目录文件!");
                            }
                        } else {
                            alert("文件不存在!");
                        }

                    }
                } else {
                    alert("用户取消目录选择行为!");
                }
            }
        });


        //Create the scroll pane and add the table to it.
//        JScrollPane scrollPane = new JScrollPane(table);
//        mainPanel.add(scrollPane);


        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        ui.setTitle("Bostan重复文件定位器");
        ui.setContentPane(ui.mainPanel);
        ui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ui.setSize(dim.getSize().width - 300, dim.getSize().height - 200);
        ui.setLocationByPlatform(true);

        ui.setLocation(dim.width / 2 - ui.getSize().width / 2, dim.height / 2 - ui.getSize().height / 2);
        ui.setVisible(true);
    }

    public static void main(String[] args) {
        final DiffUI ui = new DiffUI();
        ui.init();
    }

    private static void copyFileUsingApacheCommonsIO(File source, File dest)
            throws IOException {
        FileUtils.copyFile(source, dest);
    }


}



