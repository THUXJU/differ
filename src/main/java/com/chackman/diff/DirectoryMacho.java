package com.chackman.diff;


import org.apache.commons.io.IOUtils;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: yusup
 * Date: 4/21/14
 * Time: 19:01
 */
public class DirectoryMacho extends Thread {

    private File directory;

    private Map<Long, List<File>> filesByLength = new HashMap<Long, List<File>>();

    private static Map<Long, List<File>> filesWithSameLengthContent = new HashMap<Long, List<File>>();

    private List<File> uniqueFiles = new ArrayList<File>();


    Style style;

    public File getDirectory() {
        return directory;
    }

    public void setDirectory(File directory) {
        this.directory = directory;
    }

    public DirectoryMacho(File directory) {
        this.directory = directory;
    }

    public DirectoryMacho(File directory, JTextPane textPane) {
        this.textPane = textPane;
        this.directory = directory;
        this.document = textPane.getStyledDocument();
        style = textPane.addStyle("Bostan Style", null);
    }

    private JTextPane textPane;

    public static Map<Long, List<File>> getUniqueFilesMap() {
        return filesWithSameLengthContent;
    }


    private StyledDocument document;


    public StyledDocument getDocument() {
        return document;
    }

    public void setDocument(StyledDocument document) {
        this.document = document;
    }

    @Override
    public void run() {
        filesWithSameLengthContent.clear();
        textPane.setText("");
        if (directory.canRead()) {
            File[] children = directory.listFiles();
            if (children.length == 0) {
                DiffBroadcaster.addMessages("该目录为空!");
            } else if (children.length > 0) {
                /**
                 *  sort files by length
                 */
                for (int i = 0; i < children.length; i++) {
                    File child = children[i];

                    if (child.isDirectory()) {
                        continue;
                    }

                    if (child.length() > 0) { // 空文件忽略不计
                        if (filesByLength.containsKey(child.length())) {
                            filesByLength.get(child.length()).add(child);
                        } else {
                            List<File> fileList = new ArrayList<File>();
                            fileList.add(child);
                            filesByLength.put(child.length(), fileList);
                        }
                    }
                }

                /**
                 * ActionPotential do the hardwork here
                 */
                Iterator mapIt = filesByLength.keySet().iterator();
                while (mapIt.hasNext()) {
                    List<File> list = filesByLength.get((Long) mapIt.next()); // files with the same length
                    if (list != null && list.size() > 0) {
                        Map<File, byte[]> fileBytesMap = new HashMap<File, byte[]>();
                        for (int i = 0; i < list.size(); i++) {
                            File file = list.get(i);
                            if (file.isDirectory()) {
                                // 对于目录暂时不做任何处理
                            } else {
                                try {
                                    byte[] barr = IOUtils.toByteArray(new FileInputStream(file));
                                    fileBytesMap.put(file, barr);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    DiffBroadcaster.addMessages(e.getMessage());
                                }
                            }
                        }

                        if (list.size() == 1) {
                            filesWithSameLengthContent.put(list.get(0).length(), list);
                            continue;
                        }


                        // do the comparison and save the result to filesWithSameLengthContent
                        for (int ii = 0; ii < list.size(); ii++) {
                            for (int jj = ii + 1; jj < list.size(); jj++) {
                                File fileA = list.get(ii);
                                File fileB = list.get(jj);
                                byte[] streamABlock = fileBytesMap.get(fileA);
                                byte[] streamBBlock = fileBytesMap.get(fileB);

                                if (Arrays.equals(streamABlock, streamBBlock)) {
                                    if (filesWithSameLengthContent.get(fileA.length()) != null) {
                                        if (!filesWithSameLengthContent.get(fileA.length()).contains(fileA))
                                            filesWithSameLengthContent.get(fileA.length()).add(fileA);
                                        if (!filesWithSameLengthContent.get(fileA.length()).contains(fileB))
                                            filesWithSameLengthContent.get(fileA.length()).add(fileB);
                                    } else {
                                        List<File> fs = new ArrayList<File>();
                                        fs.add(fileA);
                                        fs.add(fileB);
                                        filesWithSameLengthContent.put(fileA.length(), fs);
                                    }
                                }
                            }
                        }
                    } else {
                        //nothing to do
                    }
                }

                Iterator resultIterator = filesWithSameLengthContent.keySet().iterator();
                while (resultIterator.hasNext()) {
                    List<File> files = filesWithSameLengthContent.get((Long) resultIterator.next());
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < files.size(); i++) {
                        File file = files.get(i);
                        sb.append(file.getName() + " " + file.length() + "\n");
                    }
                    try {
                        if (files.size() > 1) {
                            StyleConstants.setForeground(style, Color.blue);
                            document.insertString(0, sb.toString(), style);
                            document.insertString(0, "-------------------------------\n", style);

                        } else {
                            StyleConstants.setForeground(style, Color.black);
                            document.insertString(0, sb.toString(), style);
                            document.insertString(0, "-------------------------------\n", style);
                        }
                    } catch (BadLocationException e) {
                        e.printStackTrace();
                    }

                }
            }
        } else {
            DiffBroadcaster.addMessages("该目录无法读取!");
        }
    }
}

