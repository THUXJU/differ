package com.chackman.algos;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: yusup
 * Date: 4/29/14
 */
public class V100 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int start = scanner.nextInt();
        int end = scanner.nextInt();

        while (start != 0 && end != 0) {
            int max = 0;
            for (int i = start; i <= end; i++) {
                int calculatedLenght = calculateCyclicLength(i);
                if (max < calculatedLenght) {
                    max = calculatedLenght;
                }
            }
            System.out.println(start + " " + end + " " + max);
            start = scanner.nextInt();
            end = scanner.nextInt();
        }

    }

    public static int calculateCyclicLength(int n) {
        int i = 1;
        do {
            i++;
            if (n != 1) {
                if (n % 2 != 0) {
                    n = 3 * n + 1;
                } else {
                    n = n / 2;
                }
                if (n == 1) {
                }
            }
        } while (n != 1);
        return i;
    }

}
